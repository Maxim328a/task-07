package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;
	private String name;

	private Team(String name) {
		this.name = name;
		this.id = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public boolean equals(Object obj) {
		return ((Team)obj).name.equals(this.name);
	}

	@Override
	public String toString() {
		return name;
	}
}
