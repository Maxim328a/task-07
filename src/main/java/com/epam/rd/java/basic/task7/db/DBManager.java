package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

	private static DBManager instance;
//	private static final String CONNECTION_URL = "jdbc:mysql://localhost/test2db";
//	private static final String USER_LOGIN = "Maxim";
//	private static final String USER_PASS = "epam-test";
	private static String CONNECTION_URL;
	private static String USER_LOGIN;
	private static String USER_PASS;

	private static final String SELECT_ALL_USERS = "SELECT * FROM users";
	private static final String INSERT_USER = "INSERT INTO users (login) values(?)";
	private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
	private static final String INSERT_TEAM = "INSERT INTO teams (name) values(?)";
	private static final String ADD_TEAM_TO_USER = "INSERT INTO users_teams (user_id, team_id) values (?, ?)";
	private static final String GET_TEAMS_ID_FOR_USER = "SELECT team_id FROM users_teams WHERE user_id = ?";
	private static final String GET_TEAM_BY_ID = "SELECT name FROM teams WHERE id = ?";
	private static final String SET_TEAMS_FOREIGN_KEY = "ALTER TABLE users_teams ADD FOREIGN KEY (team_id) " +
			"REFERENCES teams(id) ON DELETE CASCADE";
	private static final String DELETE_TEAM_BY_ID = "DELETE FROM teams WHERE id = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name = ? where id = ?";
	private static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
	private static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	private static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";

	public static synchronized DBManager getInstance() {
		if (instance == null) instance = new DBManager();
		return instance;
	}

	private DBManager() {

	}

	private Connection getConnection() throws SQLException{
//		Connection connection = null;
//		try {
//			connection = DriverManager.getConnection(CONNECTION_URL, USER_LOGIN, USER_PASS);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return connection;
		getConnectionURLFromProperties();
		return DriverManager.getConnection(CONNECTION_URL, USER_LOGIN, USER_PASS);
	}

	private static void getConnectionURLFromProperties() {
		try (FileInputStream file = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(file);
			CONNECTION_URL = properties.getProperty("connection.url");
			USER_LOGIN = properties.getProperty("user");
			USER_PASS = properties.getProperty("password");

		} catch (IOException e) {
			// Do something
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(SELECT_ALL_USERS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				User user = User.createUser(rs.getString("login"));
				user.setId(rs.getInt("id"));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)){
			ps.setString(1, user.getLogin());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			user.setId(rs.getInt(1));
			return true;
		} catch (SQLIntegrityConstraintViolationException e) {
			user.setId(getUser(user.getLogin()).getId());
		} catch (SQLException e) {
			//e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(DELETE_USER_BY_ID)){
			for (User user:users) {
				ps.setInt(1, user.getId());
				ps.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		try (Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(GET_USER_BY_LOGIN)){
			User user = User.createUser(login);
			ps.setString(1, login);
			ResultSet rs = ps.executeQuery();
			rs.next();
			user.setId(rs.getInt(1));
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(GET_TEAM_BY_NAME)){
			Team team = Team.createTeam(name);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			rs.next();
			team.setId(rs.getInt(1));
			return team;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(SELECT_ALL_TEAMS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)){
			ps.setString(1, team.getName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			team.setId(rs.getInt(1));
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection conn = null;
		try {
			conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(ADD_TEAM_TO_USER);
			conn.setAutoCommit(false);
			for (Team t : teams) {
				ps.setInt(1, user.getId());
				ps.setInt(2, t.getId());
				ps.execute();
			}
			conn.commit();
			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
				throw new DBException("Transaction failed", e);
			} catch (SQLException ex) {
				//throw new DBException("Rollback error", ex);
			}
			//e.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e) {
			//e.printStackTrace();
		}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		try(Connection conn = getConnection();
				PreparedStatement getTeamsPS = conn.prepareStatement(GET_TEAMS_ID_FOR_USER);
				PreparedStatement getTeamNamePS = conn.prepareStatement(GET_TEAM_BY_ID)) {
			getTeamsPS.setInt(1, user.getId());
			ResultSet teamsID = getTeamsPS.executeQuery();
			while (teamsID.next()) {
				int teamID = teamsID.getInt(1);
				getTeamNamePS.setInt(1, teamID);
				ResultSet teamName = getTeamNamePS.executeQuery();
				teamName.next();
				Team t = Team.createTeam(teamName.getString(1));
				t.setId(teamID);
				userTeams.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(DELETE_TEAM_BY_ID);
				Statement smt = conn.createStatement()) {
			smt.execute(SET_TEAMS_FOREIGN_KEY);
			ps.setInt(1, team.getId());
			ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(UPDATE_TEAM)) {
			ps.setString(1, team.getName());
			ps.setInt(2, team.getId());
			ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
